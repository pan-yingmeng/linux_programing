#include<stdio.h>
int fib(int n){
    if(n==0) return 0;
    if(n==1|n==2) return 1;
    return fib(n-1)+fib(n-2);
}
int main(int argc,char* argv[]){ 
    if(argc!=2){
        return 0;
    }
    int a=atoi(argv[1]);	
    int b=fib(a);
    printf("%d",b);
    return 1;
}