#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <unistd.h>
char* filepath="/proc/meminfo";
int main(){
	char name[16],kb[3];
	int num[23],i;
	FILE* file=fopen(filepath,"r");
	for(i=0;i<23;i++){
		fscanf(file,"%s%d%s",name,&num[i],kb);
		//printf("%s %d %s\n",name,num[i],kb);
	}
	printf("              total        used        free      shared  buff/cache   available\n");
	printf("Mem:        %d     %d      %d        %d      %d      %d\n",num[0],num[0]-num[1]-(num[3]+num[4]+num[22]),num[1],num[20],num[3]+num[4]+num[22],num[2]);
	printf("Swap:       %d       %d     %d\n",num[14],num[14]-num[15],num[15]);
}

