#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<unistd.h>
#include<arpa/inet.h>
#include<fcntl.h>
#include<sys/wait.h>
#include<errno.h>
#include<time.h>

#define BUFFER_LENGTH 1024 

int main(int argc, char* argv[]) {
	struct sockaddr_in server_addr;
	int sockfd;
	socklen_t server_addr_len = sizeof(struct sockaddr);
	char buf[BUFFER_LENGTH],rbuf[BUFFER_LENGTH];
	int keyin_fd;
	int file_flags;
	int n;
	pid_t mypid;
	mypid=getpid();
  
	if (argc != 3) {
		fputs("usage:./udpClient serverIP serverPort\n", stderr);
		exit(1);
	}
	sockfd = socket(AF_INET, SOCK_DGRAM, 0);//创建UDP套接字
	if (sockfd == -1) {
		perror("created udp socket..\n");
		exit(1);
	}
	bzero(&server_addr, sizeof(server_addr));//套接字地址结构体变量初始化清0
	server_addr.sin_family = AF_INET;//指定网络协议为IP协议
	server_addr.sin_port = htons(atoi(argv[2]));//第二个为端口号
	if (inet_aton(argv[1], &server_addr.sin_addr) == 0) {//第一个参数为地址
		perror("inet_aton");
		exit(1);
	}
	//while (1) {
		memset(rbuf, 0, BUFFER_LENGTH);
        	snprintf(rbuf,BUFFER_LENGTH,"%d",mypid);
	
		n = sendto(sockfd, rbuf, strlen(rbuf), 0, (struct sockaddr*)&server_addr, sizeof(server_addr));//给服务器发送消息
		if (n == -1) {
			perror("fail to send");
			printf("my pid is %s",rbuf);
			exit(1);
		}
		memset(buf, 0, BUFFER_LENGTH);//将buf清零
		n = recvfrom(sockfd, buf, BUFFER_LENGTH, MSG_DONTWAIT, (struct sockaddr*)&server_addr, &server_addr_len);//接收来自服务器的消息
		if (n == -1) {
			if (errno != EAGAIN) {
				perror("fail to receive");
				printf("my pid is %s",rbuf);
				exit(1);
			}
		}
		else {
			buf[n] = '\0';
			printf("Server:%s\n", buf);
			
		}
	//}
	if (close(sockfd) == -1) {//关闭套接字
		perror("fail to close");
		exit(1);
	}
	puts("UDP Client is closed!\n");
	return 0;
}
