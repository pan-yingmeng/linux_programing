#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>
#include<unistd.h>
#include<arpa/inet.h>
#include<fcntl.h>
#include<sys/wait.h>
#include<errno.h>
#include<time.h>
#define BUFFER_LENGTH 1024

int main(int argc, char* argv[]) {
	struct sockaddr_in server_addr, client_addr;
	int sockfd, flags;
	socklen_t client_addr_len = sizeof(struct sockaddr);
	char buf[BUFFER_LENGTH], rbuf[BUFFER_LENGTH];
	int n;
	int seqnum=0;
	if (argc != 3) {
		fputs("usage: ./udpserver serverIP serverPort\n", stderr);
		exit(1);
	}
	sockfd = socket(AF_INET, SOCK_DGRAM, 0);//创建UDP套接字
	if (sockfd == -1) {
		perror("created UCP socket");
		exit(1);
	}
	bzero(&server_addr, sizeof(server_addr));//套接字地址信息结构体变量初始化清0
	server_addr.sin_family = AF_INET;//指定网络协议为IP协议
	server_addr.sin_port = htons(atoi(argv[2]));//第二个参数为端口号
	if (inet_aton(argv[1], &server_addr.sin_addr) == 0) {//第一个参数为地址
		perror("inet_aton");
		exit(1);
	}
	if (bind(sockfd, (struct sockaddr*)&server_addr, sizeof(server_addr)) == -1) {//绑定套接字
		memset(buf, 0, BUFFER_LENGTH);//清零
		perror("call to bind");
		exit(1);
	}
	while (1) {
		memset(buf, 0, BUFFER_LENGTH);//将buf清零
		n = recvfrom(sockfd, buf, BUFFER_LENGTH,0
, (struct sockaddr*)&client_addr, &client_addr_len);//接收来自客户端的请求
		if (n == -1) {
			if (errno != EAGAIN) {
				perror("fail to receive");
				exit(1);
			}
		}
		else {
			buf[n] = '\0';
			printf("Client:%s\n", buf);
		}
		memset(rbuf, 0, BUFFER_LENGTH);
		snprintf(rbuf, BUFFER_LENGTH,"%d",seqnum++);
		n = sendto(sockfd, rbuf, strlen(rbuf), 0, (struct sockaddr*)&client_addr, sizeof(client_addr));
		if (n == -1) {
			perror("fail to reply ");
			exit(1);
		}

	}
	if (close(sockfd) == -1) {//关闭套接字
		perror("fail to close");
		exit(1);
	}
	puts("UDP Server is closed!\n");
	return 0;
}

